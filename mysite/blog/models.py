from django.db import models
from django.utils import timezone

class Author(models.Model):
	name = models.CharField(max_length=255)

	def __str__(self):
		return self.name

class Post(models.Model):
	author = models.ForeignKey(Author, on_delete=models.CASCADE)
	title = models.CharField(max_length=200)
	text = models.TextField()
	created_date = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.title